from flask_restful import Resource
from flask_restful import reqparse

from models.user import UserModel

class UserResource(Resource):

    def get(self, username):
        user = UserModel.find_by_username(username)
        return user.json() if user else {'message': 'User not found.'}, 404
