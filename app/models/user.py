from datetime import datetime

from db import db

class UserModel(db.Model):

    __tablename__ = 'users'

    id            = db.Column(db.Integer, autoincrement=True, primary_key=True)
    name          = db.Column(db.String(256), nullable=False)
    email         = db.Column(db.String(256), nullable=False, unique=True)
    username      = db.Column(db.String(256), nullable=False, unique=True)
    password      = db.Column(db.String(256), nullable=False)
    admin         = db.Column(db.Boolean, default=False, nullable=False)
    created_at    = db.Column(db.DateTime, default=datetime.utcnow, nullable=False)

    def __init__(self, name, email, username, password, admin=False):
        self.name          = name
        self.email         = email
        self.password      = password
        self.username      = username
        self.admin         = admin
        self.created_at    = datetime.now()

    def save(self):
        db.session.add(self)
        db.session.commit()

    def json(self):
        return {
            'name':          self.name,
            'email':         self.email,
            'username':      self.username,
            'admin':         self.admin,
            'created_at':    str(self.created_at)
        }

    @classmethod
    def find_by_id(cls, _id):
        return cls.query.filter_by(id = _id).first()

    @classmethod
    def find_by_username(cls, _username):
        return cls.query.filter_by(username = _username).first()
