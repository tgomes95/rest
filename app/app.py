#!/usr/bin/env python
from flask import Flask

from flask_migrate import Migrate

from flask_restful import Api

from db import db

import config

app = Flask(__name__)
app.config.from_object(config.Development)

db.init_app(app)
migrate = Migrate(app, db)

api = Api(app)

from resources.register import RegisterResource
from resources.user import UserResource

api.add_resource(RegisterResource, '/register')
api.add_resource(UserResource, '/user/<string:username>')

if __name__ == '__main__':
    app.run(host=config.Development.HOST, port=config.Development.PORT)
