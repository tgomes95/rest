import os

SQLITE_DATABSE_URI_BASE = 'sqlite:///'
DATABASE_NAME           = 'database'

class Config:
    DEBUG                          = True
    TESTING                        = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SECRET_KEY                     = 'VlWNX@&7:d5n4<r'
    HOST                           = '0.0.0.0'
    PORT                           = '8000'

class Development(Config):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = SQLITE_DATABSE_URI_BASE + DATABASE_NAME + '.db'

# class Production(Config):
#     DEBUG = False
#     TESTING = False
#     DB_NAME = os.environ['DB_NAME']
#     DB_USER = os.environ['DB_USER']
#     DB_PASS = os.environ['DB_PASS']
#     DB_PORT = os.environ['DB_PORT']
#     DB_SERVICE = os.environ['DB_SERVICE']
#     SQLALCHEMY_DATABASE_URI = 'postgresql://{0}:{1}@{2}:{3}/{4}'.format(DB_USER, DB_PASS, DB_SERVICE, DB_PORT, DB_NAME)
